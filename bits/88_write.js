//kf_rep

function write_cfb_ctr(cfb, o) {
  switch (o.type) {
    case 'base64':
    case 'binary':
      break;
    case 'buffer':
    case 'array':
      o.type = '';
      break;
    case 'file':
      return write_dl(
        o.file,
        CFB.write(cfb, { type: has_buf ? 'buffer' : '' })
      );
    case 'string':
      throw new Error(
        "'string' output type invalid for '" + o.bookType + "' files"
      );
    default:
      throw new Error('Unrecognized type ' + o.type);
  }
  return CFB.write(cfb, o);
}

function write_zip_type(wb, opts) {
  var o = opts || {};
  style_builder = new StyleBuilder(opts);
  var z = write_zip(wb, o);
  var oopts = {};
  if (o.compression) oopts.compression = 'DEFLATE';
  if (o.password) oopts.type = has_buf ? 'nodebuffer' : 'string';
  else
    switch (o.type) {
      case 'base64':
        oopts.type = 'base64';
        break;
      case 'binary':
        oopts.type = 'string';
        break;
      case 'string':
        throw new Error(
          "'string' output type invalid for '" + o.bookType + "' files"
        );
      case 'buffer':
      case 'file':
        oopts.type = has_buf ? 'nodebuffer' : 'string';
        break;
      default:
        throw new Error('Unrecognized type ' + o.type);
    }
  var out = z.FullPaths
    ? CFB.write(z, {
        fileType: 'zip',
        type:
          { nodebuffer: 'buffer', string: 'binary' }[oopts.type] || oopts.type
      })
    : z.generate(oopts);
  /*jshint -W083 */
  if (o.password && typeof encrypt_agile !== 'undefined')
    return write_cfb_ctr(encrypt_agile(out, o.password), o); // eslint-disable-line no-undef
  /*jshint +W083 */
  if (o.type === 'file') return write_dl(o.file, out);
  return o.type == 'string' ? utf8read(out) : out;
}

// function write_cfb_type(wb, opts) {
//   var o = opts || {};
//   var cfb = write_xlscfb(wb, o);
//   return write_cfb_ctr(cfb, o);
// }

function writeSync(wb, opts) {
  var o = opts || {};
  switch (o.bookType) {
    case 'xml':
      return write_xlml(wb, o);
    default:
      return write_zip_type(wb, o);
  }
}

function writeFileSync(wb, filename, opts) {
  var o = opts || {};
  o.type = 'file';

  o.file = filename;
  switch (o.file.substr(-5).toLowerCase()) {
    case '.xlsx':
      o.bookType = 'xlsx';
      break;
    case '.xlsm':
      o.bookType = 'xlsm';
      break;
    case '.xlsb':
      o.bookType = 'xlsb';
      break;
    default:
      switch (o.file.substr(-4).toLowerCase()) {
        case '.xls':
          o.bookType = 'xls';
          break;
        case '.xml':
          o.bookType = 'xml';
          break;
      }
  }
  return writeSync(wb, o);
}
