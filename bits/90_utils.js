function decode_row(rowstr /*:string*/) /*:number*/ {
  return parseInt(unfix_row(rowstr), 10) - 1;
}
function encode_row(row /*:number*/) /*:string*/ {
  return '' + (row + 1);
}
function fix_row(cstr /*:string*/) /*:string*/ {
  return cstr.replace(/([A-Z]|^)(\d+)$/, '$1$$$2');
}
function unfix_row(cstr /*:string*/) /*:string*/ {
  return cstr.replace(/\$(\d+)$/, '$1');
}

function decode_col(colstr /*:string*/) /*:number*/ {
  var c = unfix_col(colstr),
    d = 0,
    i = 0;
  for (; i !== c.length; ++i) d = 26 * d + c.charCodeAt(i) - 64;
  return d - 1;
}
function encode_col(col /*:number*/) /*:string*/ {
  if (col < 0) throw new Error('invalid column ' + col);
  var s = '';
  for (++col; col; col = Math.floor((col - 1) / 26))
    s = String.fromCharCode(((col - 1) % 26) + 65) + s;
  return s;
}
function fix_col(cstr /*:string*/) /*:string*/ {
  return cstr.replace(/^([A-Z])/, '$$$1');
}
function unfix_col(cstr /*:string*/) /*:string*/ {
  return cstr.replace(/^\$([A-Z])/, '$1');
}

function split_cell(cstr /*:string*/) /*:Array<string>*/ {
  return cstr.replace(/(\$?[A-Z]*)(\$?\d*)/, '$1,$2').split(',');
}
function decode_cell(cstr /*:string*/) /*:CellAddress*/ {
  var splt = split_cell(cstr);
  return { c: decode_col(splt[0]), r: decode_row(splt[1]) };
}
function encode_cell(cell /*:CellAddress*/) /*:string*/ {
  return encode_col(cell.c) + encode_row(cell.r);
}
function decode_range(range /*:string*/) /*:Range*/ {
  var x = range.split(':').map(decode_cell);
  return { s: x[0], e: x[x.length - 1] };
}
/*# if only one arg, it is assumed to be a Range.  If 2 args, both are cell addresses */
function encode_range(
  cs /*:CellAddrSpec|Range*/,
  ce /*:?CellAddrSpec*/
) /*:string*/ {
  if (typeof ce === 'undefined' || typeof ce === 'number') {
    /*:: if(!(cs instanceof Range)) throw "unreachable"; */
    return encode_range(cs.s, cs.e);
  }
  /*:: if((cs instanceof Range)) throw "unreachable"; */
  if (typeof cs !== 'string') cs = encode_cell((cs /*:any*/));
  if (typeof ce !== 'string') ce = encode_cell((ce /*:any*/));
  /*:: if(typeof cs !== 'string') throw "unreachable"; */
  /*:: if(typeof ce !== 'string') throw "unreachable"; */
  return cs == ce ? cs : cs + ':' + ce;
}

function safe_decode_range(range /*:string*/) /*:Range*/ {
  var o = { s: { c: 0, r: 0 }, e: { c: 0, r: 0 } };
  var idx = 0,
    i = 0,
    cc = 0;
  var len = range.length;
  for (idx = 0; i < len; ++i) {
    if ((cc = range.charCodeAt(i) - 64) < 1 || cc > 26) break;
    idx = 26 * idx + cc;
  }
  o.s.c = --idx;

  for (idx = 0; i < len; ++i) {
    if ((cc = range.charCodeAt(i) - 48) < 0 || cc > 9) break;
    idx = 10 * idx + cc;
  }
  o.s.r = --idx;

  if (i === len || range.charCodeAt(++i) === 58) {
    o.e.c = o.s.c;
    o.e.r = o.s.r;
    return o;
  }

  for (idx = 0; i != len; ++i) {
    if ((cc = range.charCodeAt(i) - 64) < 1 || cc > 26) break;
    idx = 26 * idx + cc;
  }
  o.e.c = --idx;

  for (idx = 0; i != len; ++i) {
    if ((cc = range.charCodeAt(i) - 48) < 0 || cc > 9) break;
    idx = 10 * idx + cc;
  }
  o.e.r = --idx;
  return o;
}

function safe_format_cell(cell /*:Cell*/, v /*:any*/) {
  var q = cell.t == 'd' && v instanceof Date;
  if (cell.z != null)
    try {
      return (cell.w = SSF.format(cell.z, q ? datenum(v) : v));
    } catch (e) {}
  try {
    return (cell.w = SSF.format(
      (cell.XF || {}).numFmtId || (q ? 14 : 0),
      q ? datenum(v) : v
    ));
  } catch (e) {
    return '' + v;
  }
}

function format_cell(cell /*:Cell*/, v /*:any*/, o /*:any*/) {
  if (cell == null || cell.t == null || cell.t == 'z') return '';
  if (cell.w !== undefined) return cell.w;
  if (cell.t == 'd' && !cell.z && o && o.dateNF) cell.z = o.dateNF;
  if (v == undefined) return safe_format_cell(cell, cell.v);
  return safe_format_cell(cell, v);
}

function sheet_to_workbook(sheet /*:Worksheet*/, opts) /*:Workbook*/ {
  var n = opts && opts.sheet ? opts.sheet : 'Sheet1';
  var sheets = {};
  sheets[n] = sheet;
  return { SheetNames: [n], Sheets: sheets };
}

function sheet_add_aoa(
  _ws /*:?Worksheet*/,
  data /*:AOA*/,
  opts /*:?any*/
) /*:Worksheet*/ {
  var o = opts || {};
  var dense = _ws ? Array.isArray(_ws) : o.dense;
  if (DENSE != null && dense == null) dense = DENSE;
  var ws /*:Worksheet*/ = _ws || (dense ? ([] /*:any*/) : ({} /*:any*/));
  var _R = 0,
    _C = 0;
  if (ws && o.origin != null) {
    if (typeof o.origin == 'number') _R = o.origin;
    else {
      var _origin /*:CellAddress*/ =
        typeof o.origin == 'string' ? decode_cell(o.origin) : o.origin;
      _R = _origin.r;
      _C = _origin.c;
    }
  }
  var range /*:Range*/ = ({
    s: { c: 10000000, r: 10000000 },
    e: { c: 0, r: 0 }
  } /*:any*/);
  if (ws['!ref']) {
    var _range = safe_decode_range(ws['!ref']);
    range.s.c = _range.s.c;
    range.s.r = _range.s.r;
    range.e.c = Math.max(range.e.c, _range.e.c);
    range.e.r = Math.max(range.e.r, _range.e.r);
    if (_R == -1) range.e.r = _R = _range.e.r + 1;
  }
  for (var R = 0; R != data.length; ++R) {
    if (!data[R]) continue;
    if (!Array.isArray(data[R]))
      throw new Error('aoa_to_sheet expects an array of arrays');
    for (var C = 0; C != data[R].length; ++C) {
      if (typeof data[R][C] === 'undefined') continue;
      var cell /*:Cell*/ = ({ v: data[R][C] } /*:any*/);
      var __R = _R + R,
        __C = _C + C;
      if (range.s.r > __R) range.s.r = __R;
      if (range.s.c > __C) range.s.c = __C;
      if (range.e.r < __R) range.e.r = __R;
      if (range.e.c < __C) range.e.c = __C;
      if (
        data[R][C] &&
        typeof data[R][C] === 'object' &&
        !Array.isArray(data[R][C]) &&
        !(data[R][C] instanceof Date)
      )
        cell = data[R][C];
      else {
        if (Array.isArray(cell.v)) {
          cell.f = data[R][C][1];
          cell.v = cell.v[0];
        }
        if (cell.v === null) {
          if (cell.f) cell.t = 'n';
          else if (!o.sheetStubs) continue;
          else cell.t = 'z';
        } else if (typeof cell.v === 'number') cell.t = 'n';
        else if (typeof cell.v === 'boolean') cell.t = 'b';
        else if (cell.v instanceof Date) {
          cell.z = o.dateNF || SSF._table[14];
          if (o.cellDates) {
            cell.t = 'd';
            cell.w = SSF.format(cell.z, datenum(cell.v));
          } else {
            cell.t = 'n';
            cell.v = datenum(cell.v);
            cell.w = SSF.format(cell.z, cell.v);
          }
        } else cell.t = 's';
      }
      if (dense) {
        if (!ws[__R]) ws[__R] = [];
        if (ws[__R][__C] && ws[__R][__C].z) cell.z = ws[__R][__C].z;
        ws[__R][__C] = cell;
      } else {
        var cell_ref = encode_cell(({ c: __C, r: __R } /*:any*/));
        if (ws[cell_ref] && ws[cell_ref].z) cell.z = ws[cell_ref].z;
        ws[cell_ref] = cell;
      }
    }
  }
  if (range.s.c < 10000000) ws['!ref'] = encode_range(range);
  return ws;
}
function aoa_to_sheet(data /*:AOA*/, opts /*:?any*/) /*:Worksheet*/ {
  return sheet_add_aoa(null, data, opts);
}

/* note: browser DOM element cannot see mso- style attrs, must parse */
var HTML_ = (function () {
  function html_to_sheet(str /*:string*/, _opts) /*:Workbook*/ {
    var opts = _opts || {};
    if (DENSE != null && opts.dense == null) opts.dense = DENSE;
    var ws /*:Worksheet*/ = opts.dense ? ([] /*:any*/) : ({} /*:any*/);
    str = str.replace(/<!--.*?-->/g, '');
    var mtch /*:any*/ = str.match(/<table/i);
    if (!mtch) throw new Error('Invalid HTML: could not find <table>');
    var mtch2 /*:any*/ = str.match(/<\/table/i);
    var i /*:number*/ = mtch.index,
      j /*:number*/ = (mtch2 && mtch2.index) || str.length;
    var rows = split_regex(str.slice(i, j), /(:?<tr[^>]*>)/i, '<tr>');
    var R = -1,
      C = 0,
      RS = 0,
      CS = 0;
    var range /*:Range*/ = {
      s: { r: 10000000, c: 10000000 },
      e: { r: 0, c: 0 }
    };
    var merges /*:Array<Range>*/ = [];
    for (i = 0; i < rows.length; ++i) {
      var row = rows[i].trim();
      var hd = row.slice(0, 3).toLowerCase();
      if (hd == '<tr') {
        ++R;
        if (opts.sheetRows && opts.sheetRows <= R) {
          --R;
          break;
        }
        C = 0;
        continue;
      }
      if (hd != '<td' && hd != '<th') continue;
      var cells = row.split(/<\/t[dh]>/i);
      for (j = 0; j < cells.length; ++j) {
        var cell = cells[j].trim();
        if (!cell.match(/<t[dh]/i)) continue;
        var m = cell,
          cc = 0;
        /* TODO: parse styles etc */
        while (m.charAt(0) == '<' && (cc = m.indexOf('>')) > -1)
          m = m.slice(cc + 1);
        for (var midx = 0; midx < merges.length; ++midx) {
          var _merge /*:Range*/ = merges[midx];
          if (_merge.s.c == C && _merge.s.r < R && R <= _merge.e.r) {
            C = _merge.e.c + 1;
            midx = -1;
          }
        }
        var tag = parsexmltag(cell.slice(0, cell.indexOf('>')));
        CS = tag.colspan ? +tag.colspan : 1;
        if ((RS = +tag.rowspan) > 1 || CS > 1)
          merges.push({
            s: { r: R, c: C },
            e: { r: R + (RS || 1) - 1, c: C + CS - 1 }
          });
        var _t /*:string*/ = tag.t || '';
        /* TODO: generate stub cells */
        if (!m.length) {
          C += CS;
          continue;
        }
        m = htmldecode(m);
        if (range.s.r > R) range.s.r = R;
        if (range.e.r < R) range.e.r = R;
        if (range.s.c > C) range.s.c = C;
        if (range.e.c < C) range.e.c = C;
        if (!m.length) continue;
        var o /*:Cell*/ = { t: 's', v: m };
        if (opts.raw || !m.trim().length || _t == 's') {
        } else if (m === 'TRUE') o = { t: 'b', v: true };
        else if (m === 'FALSE') o = { t: 'b', v: false };
        else if (!isNaN(fuzzynum(m))) o = { t: 'n', v: fuzzynum(m) };
        else if (!isNaN(fuzzydate(m).getDate())) {
          o = ({ t: 'd', v: parseDate(m) } /*:any*/);
          if (!opts.cellDates) o = ({ t: 'n', v: datenum(o.v) } /*:any*/);
          o.z = opts.dateNF || SSF._table[14];
        }
        if (opts.dense) {
          if (!ws[R]) ws[R] = [];
          ws[R][C] = o;
        } else ws[encode_cell({ r: R, c: C })] = o;
        C += CS;
      }
    }
    ws['!ref'] = encode_range(range);
    if (merges.length) ws['!merges'] = merges;
    return ws;
  }
  function html_to_book(str /*:string*/, opts) /*:Workbook*/ {
    return sheet_to_workbook(html_to_sheet(str, opts), opts);
  }
  function make_html_row(
    ws /*:Worksheet*/,
    r /*:Range*/,
    R /*:number*/,
    o /*:Sheet2HTMLOpts*/
  ) /*:string*/ {
    var M /*:Array<Range>*/ = ws['!merges'] || [];
    var oo /*:Array<string>*/ = [];
    for (var C = r.s.c; C <= r.e.c; ++C) {
      var RS = 0,
        CS = 0;
      for (var j = 0; j < M.length; ++j) {
        if (M[j].s.r > R || M[j].s.c > C) continue;
        if (M[j].e.r < R || M[j].e.c < C) continue;
        if (M[j].s.r < R || M[j].s.c < C) {
          RS = -1;
          break;
        }
        RS = M[j].e.r - M[j].s.r + 1;
        CS = M[j].e.c - M[j].s.c + 1;
        break;
      }
      if (RS < 0) continue;
      var coord = encode_cell({ r: R, c: C });
      var cell = o.dense ? (ws[R] || [])[C] : ws[coord];
      /* TODO: html entities */
      var w =
        (cell &&
          cell.v != null &&
          (cell.h ||
            escapehtml(cell.w || (format_cell(cell), cell.w) || ''))) ||
        '';
      var sp = ({} /*:any*/);
      if (RS > 1) sp.rowspan = RS;
      if (CS > 1) sp.colspan = CS;
      sp.t = (cell && cell.t) || 'z';
      if (o.editable) w = '<span contenteditable="true">' + w + '</span>';
      sp.id = (o.id || 'sjs') + '-' + coord;
      oo.push(writextag('td', w, sp));
    }
    var preamble = '<tr>';
    return preamble + oo.join('') + '</tr>';
  }
  function make_html_preamble(
    ws /*:Worksheet*/,
    R /*:Range*/,
    o /*:Sheet2HTMLOpts*/
  ) /*:string*/ {
    var out /*:Array<string>*/ = [];
    return (
      out.join('') + '<table' + (o && o.id ? ' id="' + o.id + '"' : '') + '>'
    );
  }
  var _BEGIN =
    '<html><head><meta charset="utf-8"/><title>SheetJS Table Export</title></head><body>';
  var _END = '</body></html>';
  function sheet_to_html(
    ws /*:Worksheet*/,
    opts /*:?Sheet2HTMLOpts*/ /*, wb:?Workbook*/
  ) /*:string*/ {
    var o = opts || {};
    var header = o.header != null ? o.header : _BEGIN;
    var footer = o.footer != null ? o.footer : _END;
    var out /*:Array<string>*/ = [header];
    var r = decode_range(ws['!ref']);
    o.dense = Array.isArray(ws);
    out.push(make_html_preamble(ws, r, o));
    for (var R = r.s.r; R <= r.e.r; ++R) out.push(make_html_row(ws, r, R, o));
    out.push('</table>' + footer);
    return out.join('');
  }

  return {
    to_workbook: html_to_book,
    to_sheet: html_to_sheet,
    _row: make_html_row,
    BEGIN: _BEGIN,
    END: _END,
    _preamble: make_html_preamble,
    from_sheet: sheet_to_html
  };
})();

function parse_dom_table(
  table /*:HTMLElement*/,
  _opts /*:?any*/
) /*:Worksheet*/ {
  var opts = _opts || {};
  if (DENSE != null) opts.dense = DENSE;
  var ws /*:Worksheet*/ = opts.dense ? ([] /*:any*/) : ({} /*:any*/);
  var rows /*:HTMLCollection<HTMLTableRowElement>*/ = table.getElementsByTagName(
    'tr'
  );
  var sheetRows = opts.sheetRows || 10000000;
  var range /*:Range*/ = { s: { r: 0, c: 0 }, e: { r: 0, c: 0 } };
  var merges /*:Array<Range>*/ = [],
    midx = 0;
  var rowinfo /*:Array<RowInfo>*/ = [];
  var _R = 0,
    R = 0,
    _C = 0,
    C = 0,
    RS = 0,
    CS = 0;
  for (; _R < rows.length && R < sheetRows; ++_R) {
    var row /*:HTMLTableRowElement*/ = rows[_R];
    if (is_dom_element_hidden(row)) {
      if (opts.display) continue;
      rowinfo[R] = { hidden: true };
    }
    var elts /*:HTMLCollection<HTMLTableCellElement>*/ =
      (row.children /*:any*/);
    for (_C = C = 0; _C < elts.length; ++_C) {
      var elt /*:HTMLTableCellElement*/ = elts[_C];
      if (opts.display && is_dom_element_hidden(elt)) continue;
      var v /*:string*/ = htmldecode(elt.innerHTML);
      for (midx = 0; midx < merges.length; ++midx) {
        var m /*:Range*/ = merges[midx];
        if (m.s.c == C && m.s.r <= R && R <= m.e.r) {
          C = m.e.c + 1;
          midx = -1;
        }
      }
      /* TODO: figure out how to extract nonstandard mso- style */
      CS = +elt.getAttribute('colspan') || 1;
      if ((RS = +elt.getAttribute('rowspan')) > 0 || CS > 1)
        merges.push({
          s: { r: R, c: C },
          e: { r: R + (RS || 1) - 1, c: C + CS - 1 }
        });
      var o /*:Cell*/ = { t: 's', v: v };
      var _t /*:string*/ = elt.getAttribute('t') || '';
      if (v != null) {
        if (v.length == 0) o.t = _t || 'z';
        else if (opts.raw || v.trim().length == 0 || _t == 's') {
        } else if (v === 'TRUE') o = { t: 'b', v: true };
        else if (v === 'FALSE') o = { t: 'b', v: false };
        else if (!isNaN(fuzzynum(v))) o = { t: 'n', v: fuzzynum(v) };
        else if (!isNaN(fuzzydate(v).getDate())) {
          o = ({ t: 'd', v: parseDate(v) } /*:any*/);
          if (!opts.cellDates) o = ({ t: 'n', v: datenum(o.v) } /*:any*/);
          o.z = opts.dateNF || SSF._table[14];
        }
      }
      if (opts.dense) {
        if (!ws[R]) ws[R] = [];
        ws[R][C] = o;
      } else ws[encode_cell({ c: C, r: R })] = o;
      if (range.e.c < C) range.e.c = C;
      C += CS;
    }
    ++R;
  }
  if (merges.length) ws['!merges'] = merges;
  if (rowinfo.length) ws['!rows'] = rowinfo;
  range.e.r = R - 1;
  ws['!ref'] = encode_range(range);
  if (R >= sheetRows)
    ws['!fullref'] = encode_range(
      ((range.e.r = rows.length - _R + R - 1), range)
    ); // We can count the real number of rows to parse but we don't to improve the performance
  return ws;
}

function table_to_book(table /*:HTMLElement*/, opts /*:?any*/) /*:Workbook*/ {
  return sheet_to_workbook(parse_dom_table(table, opts), opts);
}

function is_dom_element_hidden(element /*:HTMLElement*/) /*:boolean*/ {
  var display /*:string*/ = '';
  var get_computed_style /*:?function*/ = get_get_computed_style_function(
    element
  );
  if (get_computed_style)
    display = get_computed_style(element).getPropertyValue('display');
  if (!display) display = element.style.display; // Fallback for cases when getComputedStyle is not available (e.g. an old browser or some Node.js environments) or doesn't work (e.g. if the element is not inserted to a document)
  return display === 'none';
}

/* global getComputedStyle */
function get_get_computed_style_function(
  element /*:HTMLElement*/
) /*:?function*/ {
  // The proper getComputedStyle implementation is the one defined in the element window
  if (
    element.ownerDocument.defaultView &&
    typeof element.ownerDocument.defaultView.getComputedStyle === 'function'
  )
    return element.ownerDocument.defaultView.getComputedStyle;
  // If it is not available, try to get one from the global namespace
  if (typeof getComputedStyle === 'function') return getComputedStyle;
  return null;
}

/*::
type MJRObject = {
	row: any;
	isempty: boolean;
};
*/
function make_json_row(
  sheet /*:Worksheet*/,
  r /*:Range*/,
  R /*:number*/,
  cols /*:Array<string>*/,
  header /*:number*/,
  hdr /*:Array<any>*/,
  dense /*:boolean*/,
  o /*:Sheet2JSONOpts*/
) /*:MJRObject*/ {
  var rr = encode_row(R);
  var defval = o.defval,
    raw = o.raw || !Object.prototype.hasOwnProperty.call(o, 'raw');
  var isempty = true;
  var row /*:any*/ = header === 1 ? [] : {};
  if (header !== 1) {
    if (Object.defineProperty)
      try {
        Object.defineProperty(row, '__rowNum__', {
          value: R,
          enumerable: false
        });
      } catch (e) {
        row.__rowNum__ = R;
      }
    else row.__rowNum__ = R;
  }
  if (!dense || sheet[R])
    for (var C = r.s.c; C <= r.e.c; ++C) {
      var val = dense ? sheet[R][C] : sheet[cols[C] + rr];
      if (val === undefined || val.t === undefined) {
        if (defval === undefined) continue;
        if (hdr[C] != null) {
          row[hdr[C]] = defval;
        }
        continue;
      }
      var v = val.v;
      switch (val.t) {
        case 'z':
          if (v == null) break;
          continue;
        case 'e':
          v = void 0;
          break;
        case 's':
        case 'd':
        case 'b':
        case 'n':
          break;
        default:
          throw new Error('unrecognized type ' + val.t);
      }
      if (hdr[C] != null) {
        if (v == null) {
          if (defval !== undefined) row[hdr[C]] = defval;
          else if (raw && v === null) row[hdr[C]] = null;
          else continue;
        } else {
          row[hdr[C]] = raw ? v : format_cell(val, v, o);
        }
        if (v != null) isempty = false;
      }
    }
  return { row: row, isempty: isempty };
}

function sheet_to_json(sheet /*:Worksheet*/, opts /*:?Sheet2JSONOpts*/) {
  if (sheet == null || sheet['!ref'] == null) return [];
  var val = { t: 'n', v: 0 },
    header = 0,
    offset = 1,
    hdr /*:Array<any>*/ = [],
    v = 0,
    vv = '';
  var r = { s: { r: 0, c: 0 }, e: { r: 0, c: 0 } };
  var o = opts || {};
  var range = o.range != null ? o.range : sheet['!ref'];
  if (o.header === 1) header = 1;
  else if (o.header === 'A') header = 2;
  else if (Array.isArray(o.header)) header = 3;
  else if (o.header == null) header = 0;
  switch (typeof range) {
    case 'string':
      r = safe_decode_range(range);
      break;
    case 'number':
      r = safe_decode_range(sheet['!ref']);
      r.s.r = range;
      break;
    default:
      r = range;
  }
  if (header > 0) offset = 0;
  var rr = encode_row(r.s.r);
  var cols /*:Array<string>*/ = [];
  var out /*:Array<any>*/ = [];
  var outi = 0,
    counter = 0;
  var dense = Array.isArray(sheet);
  var R = r.s.r,
    C = 0,
    CC = 0;
  if (dense && !sheet[R]) sheet[R] = [];
  for (C = r.s.c; C <= r.e.c; ++C) {
    cols[C] = encode_col(C);
    val = dense ? sheet[R][C] : sheet[cols[C] + rr];
    switch (header) {
      case 1:
        hdr[C] = C - r.s.c;
        break;
      case 2:
        hdr[C] = cols[C];
        break;
      case 3:
        hdr[C] = o.header[C - r.s.c];
        break;
      default:
        if (val == null) val = { w: '__EMPTY', t: 's' };
        vv = v = format_cell(val, null, o);
        counter = 0;
        for (CC = 0; CC < hdr.length; ++CC)
          if (hdr[CC] == vv) vv = v + '_' + ++counter;
        hdr[C] = vv;
    }
  }
  for (R = r.s.r + offset; R <= r.e.r; ++R) {
    var row = make_json_row(sheet, r, R, cols, header, hdr, dense, o);
    if (
      row.isempty === false ||
      (header === 1 ? o.blankrows !== false : !!o.blankrows)
    )
      out[outi++] = row.row;
  }
  out.length = outi;
  return out;
}

var qreg = /"/g;
function make_csv_row(
  sheet /*:Worksheet*/,
  r /*:Range*/,
  R /*:number*/,
  cols /*:Array<string>*/,
  fs /*:number*/,
  rs /*:number*/,
  FS /*:string*/,
  o /*:Sheet2CSVOpts*/
) /*:?string*/ {
  var isempty = true;
  var row /*:Array<string>*/ = [],
    txt = '',
    rr = encode_row(R);
  for (var C = r.s.c; C <= r.e.c; ++C) {
    if (!cols[C]) continue;
    var val = o.dense ? (sheet[R] || [])[C] : sheet[cols[C] + rr];
    if (val == null) txt = '';
    else if (val.v != null) {
      isempty = false;
      txt = '' + format_cell(val, null, o);
      for (var i = 0, cc = 0; i !== txt.length; ++i)
        if ((cc = txt.charCodeAt(i)) === fs || cc === rs || cc === 34) {
          txt = '"' + txt.replace(qreg, '""') + '"';
          break;
        }
      if (txt == 'ID') txt = '"ID"';
    } else if (val.f != null && !val.F) {
      isempty = false;
      txt = '=' + val.f;
      if (txt.indexOf(',') >= 0) txt = '"' + txt.replace(qreg, '""') + '"';
    } else txt = '';
    /* NOTE: Excel CSV does not support array formulae */
    row.push(txt);
  }
  if (o.blankrows === false && isempty) return null;
  return row.join(FS);
}

function sheet_to_csv(
  sheet /*:Worksheet*/,
  opts /*:?Sheet2CSVOpts*/
) /*:string*/ {
  var out /*:Array<string>*/ = [];
  var o = opts == null ? {} : opts;
  if (sheet == null || sheet['!ref'] == null) return '';
  var r = safe_decode_range(sheet['!ref']);
  var FS = o.FS !== undefined ? o.FS : ',',
    fs = FS.charCodeAt(0);
  var RS = o.RS !== undefined ? o.RS : '\n',
    rs = RS.charCodeAt(0);
  var endregex = new RegExp((FS == '|' ? '\\|' : FS) + '+$');
  var row = '',
    cols /*:Array<string>*/ = [];
  o.dense = Array.isArray(sheet);
  var colinfo /*:Array<ColInfo>*/ = (o.skipHidden && sheet['!cols']) || [];
  var rowinfo /*:Array<ColInfo>*/ = (o.skipHidden && sheet['!rows']) || [];
  for (var C = r.s.c; C <= r.e.c; ++C)
    if (!(colinfo[C] || {}).hidden) cols[C] = encode_col(C);
  for (var R = r.s.r; R <= r.e.r; ++R) {
    if ((rowinfo[R] || {}).hidden) continue;
    row = make_csv_row(sheet, r, R, cols, fs, rs, FS, o);
    if (row == null) {
      continue;
    }
    if (o.strip) row = row.replace(endregex, '');
    out.push(row + RS);
  }
  delete o.dense;
  return out.join('');
}

function sheet_to_txt(sheet /*:Worksheet*/, opts /*:?Sheet2CSVOpts*/) {
  if (!opts) opts = {};
  opts.FS = '\t';
  opts.RS = '\n';
  var s = sheet_to_csv(sheet, opts);
  if (typeof cptable == 'undefined' || opts.type == 'string') return s;
  var o = cptable.utils.encode(1200, s, 'str');
  return String.fromCharCode(255) + String.fromCharCode(254) + o;
}

function sheet_to_formulae(sheet /*:Worksheet*/) /*:Array<string>*/ {
  var y = '',
    x,
    val = '';
  if (sheet == null || sheet['!ref'] == null) return [];
  var r = safe_decode_range(sheet['!ref']),
    rr = '',
    cols /*:Array<string>*/ = [],
    C;
  var cmds /*:Array<string>*/ = [];
  var dense = Array.isArray(sheet);
  for (C = r.s.c; C <= r.e.c; ++C) cols[C] = encode_col(C);
  for (var R = r.s.r; R <= r.e.r; ++R) {
    rr = encode_row(R);
    for (C = r.s.c; C <= r.e.c; ++C) {
      y = cols[C] + rr;
      x = dense ? (sheet[R] || [])[C] : sheet[y];
      val = '';
      if (x === undefined) continue;
      else if (x.F != null) {
        y = x.F;
        if (!x.f) continue;
        val = x.f;
        if (y.indexOf(':') == -1) y = y + ':' + y;
      }
      if (x.f != null) val = x.f;
      else if (x.t == 'z') continue;
      else if (x.t == 'n' && x.v != null) val = '' + x.v;
      else if (x.t == 'b') val = x.v ? 'TRUE' : 'FALSE';
      else if (x.w !== undefined) val = "'" + x.w;
      else if (x.v === undefined) continue;
      else if (x.t == 's') val = "'" + x.v;
      else val = '' + x.v;
      cmds[cmds.length] = y + '=' + val;
    }
  }
  return cmds;
}

function sheet_add_json(
  _ws /*:?Worksheet*/,
  js /*:Array<any>*/,
  opts
) /*:Worksheet*/ {
  var o = opts || {};
  var offset = +!o.skipHeader;
  var ws /*:Worksheet*/ = _ws || ({} /*:any*/);
  var _R = 0,
    _C = 0;
  if (ws && o.origin != null) {
    if (typeof o.origin == 'number') _R = o.origin;
    else {
      var _origin /*:CellAddress*/ =
        typeof o.origin == 'string' ? decode_cell(o.origin) : o.origin;
      _R = _origin.r;
      _C = _origin.c;
    }
  }
  var cell /*:Cell*/;
  var range /*:Range*/ = ({
    s: { c: 0, r: 0 },
    e: { c: _C, r: _R + js.length - 1 + offset }
  } /*:any*/);
  if (ws['!ref']) {
    var _range = safe_decode_range(ws['!ref']);
    range.e.c = Math.max(range.e.c, _range.e.c);
    range.e.r = Math.max(range.e.r, _range.e.r);
    if (_R == -1) {
      _R = range.e.r + 1;
      range.e.r = _R + js.length - 1 + offset;
    }
  }
  var hdr /*:Array<string>*/ = o.header || [],
    C = 0;

  js.forEach(function (JS, R /*:number*/) {
    keys(JS).forEach(function (k) {
      if ((C = hdr.indexOf(k)) == -1) hdr[(C = hdr.length)] = k;
      var v = JS[k];
      var t = 'z';
      var z = '';
      var ref = encode_cell({ c: _C + C, r: _R + R + offset });
      cell = utils.sheet_get_cell(ws, ref);
      if (v && typeof v === 'object' && !(v instanceof Date)) {
        ws[ref] = v;
      } else {
        if (typeof v == 'number') t = 'n';
        else if (typeof v == 'boolean') t = 'b';
        else if (typeof v == 'string') t = 's';
        else if (v instanceof Date) {
          t = 'd';
          if (!o.cellDates) {
            t = 'n';
            v = datenum(v);
          }
          z = o.dateNF || SSF._table[14];
        }
        if (!cell) ws[ref] = cell = ({ t: t, v: v } /*:any*/);
        else {
          cell.t = t;
          cell.v = v;
          delete cell.w;
          delete cell.R;
          if (z) cell.z = z;
        }
        if (z) cell.z = z;
      }
    });
  });
  range.e.c = Math.max(range.e.c, _C + hdr.length - 1);
  var __R = encode_row(_R);
  if (offset)
    for (C = 0; C < hdr.length; ++C)
      ws[encode_col(C + _C) + __R] = { t: 's', v: hdr[C] };
  ws['!ref'] = encode_range(range);
  return ws;
}
function json_to_sheet(js /*:Array<any>*/, opts) /*:Worksheet*/ {
  return sheet_add_json(null, js, opts);
}

var utils /*:any*/ = {
  encode_col: encode_col,
  encode_row: encode_row,
  encode_cell: encode_cell,
  encode_range: encode_range,
  decode_col: decode_col,
  decode_row: decode_row,
  split_cell: split_cell,
  decode_cell: decode_cell,
  decode_range: decode_range,
  format_cell: format_cell,
  get_formulae: sheet_to_formulae,
  make_csv: sheet_to_csv,
  make_json: sheet_to_json,
  make_formulae: sheet_to_formulae,
  sheet_add_aoa: sheet_add_aoa,
  sheet_add_json: sheet_add_json,
  aoa_to_sheet: aoa_to_sheet,
  json_to_sheet: json_to_sheet,
  table_to_sheet: parse_dom_table,
  table_to_book: table_to_book,
  sheet_to_csv: sheet_to_csv,
  sheet_to_txt: sheet_to_txt,
  sheet_to_json: sheet_to_json,
  sheet_to_html: HTML_.from_sheet,
  sheet_to_formulae: sheet_to_formulae,
  sheet_to_row_object_array: sheet_to_json
};
