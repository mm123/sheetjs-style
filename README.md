
# Sheetjs-style
Support setting styles for sheetjs
Based on the [js-xlsx sheetjs fork](https://github.com/protobi/js-xlsx) with extra download fixes and type declarations to be easily used with typescript.

## install
```
npm install https://gitlab.com/mm123/sheetjs-style/-/archive
```
or to fix a specific tagged version you can use the `npm install <tarball url>` option (the tag appears twice in the Gitlab URL)
```
npm install https://gitlab.com/mm123/sheetjs-style/-/archive/0.9.2/sheetjs-style-0.9.2.tar.gz
```

## How to Use?
Please read [js-xlsx Documents](https://github.com/protobi/js-xlsx/blob/master/README.md)!

For downloading / writing out files with style, you need to set the cellStyles option for `read` and `writeFile`
```js
  var reader = new FileReader();
  reader.readAsArrayBuffer(fileBuffer);
  reader.onload = (e) => {
    var data = new Uint8Array(reader.result);
    var wb = XLSX.read(data, { 
      type: 'array', 
      cellStyles: true /* make sure we get the styles processed */
    });
    var sName = 'Sheet1';
    var mySheet = wb.Sheets[sName];
    var sheetJSON =  XLSX.utils.sheet_to_json(mySheet, {
      dateNF: 22,
      defval: ''
    }); // dateNF 22 = m/d/yy hh:mm:ss and 
        // avoids potential date conversion issues

    // ... do something with the JSON

    // prepare for file output 
    
    // set header (1st row)
    var xlCloneHeader = ['StartDate', 'EndDate', 'Budget',  /*... other header text */ ];
    // convert JSON back with styles 
     var ws = XLSX.utils.json_to_sheet(sheetJSON, {
       dateNF: 22,
      header: xlCloneHeader
    });

    // optional - provide header formats 
    // simple styling 
    ws["A1"].s = { // set the style for target cell
      font: {
        name: 'Calibri',
        sz: 24,
        bold: true,
        color: { rgb: "FFFFAA00" }
      },
    };
    // for more advanced examples see the examples at 
    // https://github.com/protobi/js-xlsx/blob/master/example-style.js   
    // ...
    /* add worksheet to workbook */
    XLSX.utils.book_append_sheet(wb, ws, 'MySheet' /*MySheet = sheet name */);
    //...

    // in contrast to the older js-xlsx version, writeFile now supports
    // downloads in browser environments the same way as the newer 
    // sheetjs versions do it and FileSaver.js is no longer needed 
    XLSX.writeFile(wb, 'test_filename.xlsx', {
      bookType: 'xlsx',
      type: 'file',
      cellDates: false
    });
  }
```
`writeFile` will download the file in a browser environment, without a need for an extra library.

## Handling Webpack related issues 
### Custom Webpack configuration
Webpack seems to have an issue recognizing some dependency imports correctly, even though they work fine e.g. in a bundled JS package. 
Declaring them as external dependencies in the webpack configuration file `webpack.config.js` will address this. 
```js
  // ...
  return {
  //...
  // fix isssues with sheetjs style dependencies
    ,externals: [
      {
        './cptable': 'var cptable',
        cptable: 'var cptable',
        '../xlsx': 'var _XLSX',
        xlsx: 'var _XLSX'
      }
    ]
}
```
### Issues with Create React App (CRA)
CRA uses it's own webpack config inside the module, which you can't easily modify until you run `npm eject` (after running it you can't upgrade CRA to a newer version any longer). After ejecting you get a full webpack configuration file instead and could use the apporach above. 

To avoid ejecting and just partially patch the webpack configuration, people tend to use solutions like [react-app-rewired](https://github.com/timarney/react-app-rewired) instead. It controls the aspects to modify in a `config-override.js` file in the root folder. 

```js
module.exports = function override(config, env) {

  if (!config.externals) {
    config.externals = {};
  }

  config.externals = [
    { ...config.externals,
      './cptable': 'var cptable',
      cptable: 'var cptable',
      '../xlsx': 'var _XLSX',
      xlsx: 'var _XLSX'
    }
  ];

  return config;
};

```

## Thanks
[sheetjs](https://github.com/SheetJS/sheetjs)

[js-xlsx](https://github.com/protobi/js-xlsx)